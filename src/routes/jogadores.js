const routes = require('express').Router();

let jogadores = [
    { id: 1, name: 'Fallen', country: 'Brasileiro', team_name: 'Mibr', team_country: 'Estados Unidos', nascimento: "30 de maio de 1991", bio: "Gabriel Toledo de Alcântara Sguário, mais conhecido como FalleN, é um jogador profissional de Counter-Strike: Global Offensive que joga atualmente pela MIBR. Em 2015 ele foi nomeado como a pessoa mais influente no Esports brasileiro. " },
    { id: 2, name: 'S1mple', country: 'Ucraniano', team_name: 'Naturus Vince', team_country: 'Ucrânia', nascimento: "2 de outubro de 1997 ", bio: "Oleksandr Kostyliev, mais conhecido por seu apelido s1mple, é um jogador profissional de Counter-Strike: Global Offensive da Ucrânia. Atualmente, ele joga pelo Natus Vincere e é considerado um dos melhores jogadores da história da ofensiva global." },
    { id: 3, name: 'Dev1ce', country:'Português', team_name: 'Astralis', team_country: 'Dinamarca', nascimento: '8 de setembro de 1995', bio: 'Nicolai Reedtz, mais conhecido como dev1ce, é um profissional dinamarquês de Counter-Strike: Global Offensive. Atualmente, ele joga como AWPer para Astralis, juntamente com dupreeh, gla1ve, Xyp9x e Magisk.'},



]

routes.get('/', async (req, res, next) => {
    const formatedJogadores = jogadores.map((jogador) => {
        return { ...jogador, bio: undefined, nascimento: undefined }
    });

    res.status(200).send(formatedJogadores);
});

routes.get('/:id', async (req, res, next) => {
    const jogador = jogadores.find(jogador => jogador.id == req.params.id);

    if (!jogador) return res.status(404).send({ error: "Player not found" })

    res.status(200).send(jogador);
});

routes.post('/', async (req, res, next) => {
    const { id, name, country, bio, nascimento, team_name, team_country } = req.body;
    if (!name || !country || !bio || !nascimento || !team_name || !team_country) return res.status(400).send({ error: "Invalid form" });

    if (id) {
        jogadores = jogadores.filter(jogador => jogador.id !== id);
        jogadores. push(req.body);
        return res.status(200).send(req.body);
    }

    let lastId = 0;

    if (jogadores && jogadores.length > 0) {
        lastId = jogadores[jogadores.length - 1].id;
    }

    const jogador = { id: lastId+1, ...req.body };
    jogadores.push(jogador);

    res.status(200).send(jogador);
});

routes.delete('/:id', async (req, res, next) => {
    const id = parseInt(req.params.id);
    jogadores = jogadores.filter(jogador => jogador.id !== id);
    res.status(200).send(jogadores);
});

module.exports = app => app.use('/players', routes);